//
//  ViewController.swift
//  PListManagerTestApp
//
//  Created by Glazel Magpayo on 13/06/2016.
//  Copyright © 2016 Glazel Magpayo. All rights reserved.
//

import UIKit

//TODO : Adjust collection view for diff sizes
class HomeViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var contactList = [ContactDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPlistFile()
        refreshContactList()
        title = "Contacts"
        
        /**
         * Add Notification center
         */
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.refreshContactList), name: "GETCONTACTLIST", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * Set up plist. Create new file if plist not found.
     */
    func setUpPlistFile() {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, .UserDomainMask, true)[0]
        let directoryString   = documentDirectory as NSString
        let plistFileName     = PListManager.sharedInstance.PLIST_FILE_NAME
        var path              = directoryString.stringByAppendingPathComponent(plistFileName)
        let fileManager       = NSFileManager.defaultManager()

        //Create plist file if doesn't exists
        if !fileManager.fileExistsAtPath(path) {
            path = directoryString.stringByAppendingPathComponent(plistFileName)
            
            let data = NSMutableDictionary()
            data.writeToFile(path, atomically: true)
        }
        
        PListManager.sharedInstance.PLIST_FILE_PATH = path
        
        print("PATH: \(path)")
    }
    
    /**
     * Retrieved contacts and reload collectionView
     */
    func refreshContactList() {
        contactList = Contact().retrievedContactDetails()
        collectionView.reloadData()
    }
}

//MARK: - UICollectionViewDataSource
extension HomeViewController : UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contactList.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddContactCell", forIndexPath: indexPath)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ContactCell", forIndexPath: indexPath)
        let fullnameLabel = cell.viewWithTag(2) as! UILabel
        let contactLabel  = cell.viewWithTag(3) as! UILabel
        
        let contactDetail  = contactList[indexPath.row - 1]
        fullnameLabel.text = "\(contactDetail.firstName) \(contactDetail.lastName)"
        contactLabel.text  = "\(contactDetail.contactNumber.base64Decoder())"
        
        return cell
    }
}

//MARK: - UICollectionViewDelegate
extension HomeViewController : UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ContactFormViewController") as! ContactFormViewController
        vc.contactToEdit = indexPath.row == 0 ? nil : contactList[indexPath.row - 1]
        navigationController?.pushViewController(vc, animated: true)
    }
}

