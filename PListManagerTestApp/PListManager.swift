//
//  PListManager.swift
//  PListManagerTestApp
//
//  Created by Glazel Magpayo on 14/06/2016.
//  Copyright © 2016 Glazel Magpayo. All rights reserved.
//

import UIKit

/**
 * Storage of plist infos (filepath, filename)
 */
class PListManager: NSObject {
    var PLIST_FILE_PATH : String!
    let PLIST_FILE_NAME = "PlistManager.plist"
    
    class var sharedInstance : PListManager {
        struct Singleton {
            static let instance = PListManager()
        }
        
        return Singleton.instance
    }
}