//
//  Contact.swift
//  PListManagerTestApp
//
//  Created by Glazel Magpayo on 14/06/2016.
//  Copyright © 2016 Glazel Magpayo. All rights reserved.
//

import UIKit

struct ContactDetails {
    var id            : String?
    var firstName     = ""
    var lastName      = ""
    var contactNumber = ""
}

class Contact: NSObject {
    let FIRSTNAME   = "FirstName"
    let LASTNAME    = "LastName"
    let CONTACTNO   = "ContactNo"
    let CONTACTLIST = "ContactList"
    
    func saveNewContactDetails(newContact: ContactDetails) {
        /**
         * Get plist content
         */
        let plistRecord = NSMutableDictionary(contentsOfFile:PListManager.sharedInstance.PLIST_FILE_PATH)
        
        /**
         * Create new data dictionary
         */
        let data = NSMutableDictionary()
        data.setObject(newContact.firstName, forKey: FIRSTNAME)
        data.setObject(newContact.lastName, forKey: LASTNAME)
        data.setObject(newContact.contactNumber.base64Encoded(), forKey: CONTACTNO)
        
        /**
         * Generate object id
         */
        let objectID = String(NSDate().timeIntervalSince1970 * 1000)
        
        if let savedContactList = plistRecord?.objectForKey(CONTACTLIST) {
            /**
             * Add new data on existing data list
             */
            let contacts = savedContactList as! NSMutableDictionary
            contacts.setObject(data, forKey: objectID)
            plistRecord?.setObject(contacts, forKey: CONTACTLIST)
            plistRecord?.writeToFile(PListManager.sharedInstance.PLIST_FILE_PATH, atomically: true)
        } else {
            /**
             * Add first data to plist
             */
            let newRecord = NSMutableDictionary()
            newRecord.setObject(data, forKey: objectID)
            
            let newEntry = NSMutableDictionary()
            newEntry.setObject(newRecord, forKey: CONTACTLIST)
            
            newEntry.writeToFile(PListManager.sharedInstance.PLIST_FILE_PATH, atomically: true)
        }
    }
    
    func editContactDetails(contact: ContactDetails) {
        /**
         * Get plist content
         */
        let plistRecord = NSMutableDictionary(contentsOfFile:PListManager.sharedInstance.PLIST_FILE_PATH)
        let contactList = plistRecord?.objectForKey(CONTACTLIST) as! NSMutableDictionary
        
        let data = NSMutableDictionary()
        data.setObject(contact.firstName, forKey: FIRSTNAME)
        data.setObject(contact.lastName, forKey: LASTNAME)
        data.setObject(contact.contactNumber.base64Encoded(), forKey: CONTACTNO)
        contactList.setObject(data, forKey: contact.id!)
        
        plistRecord?.writeToFile(PListManager.sharedInstance.PLIST_FILE_PATH, atomically: true)
    }
    
    func retrievedContactDetails() -> [ContactDetails] {
        var contactList = [ContactDetails]()
        let plistRecord = NSMutableDictionary(contentsOfFile:PListManager.sharedInstance.PLIST_FILE_PATH)
        
        /**
         * Check if Contact List already exist
         */
        if let savedContactList = plistRecord?.objectForKey(CONTACTLIST) {
            let contacts = savedContactList as! NSDictionary
            
            /**
             * Convert dictionary to ContactDetails struct
             */
            for data in contacts.allKeys as! [String] {
                let contact = contacts[data] as! [String: AnyObject]
                let contactEntry = ContactDetails(id: data,
                                                firstName: String(contact[FIRSTNAME]!),
                                                  lastName: String(contact[LASTNAME]!),
                                                  contactNumber: String(contact[CONTACTNO]!))
                contactList.append(contactEntry)
            }
        }
        
        return contactList
    }
    
    func deleteContactDetails(contact: ContactDetails) {
        /**
         * Get plist content
         */
        let plistRecord = NSMutableDictionary(contentsOfFile:PListManager.sharedInstance.PLIST_FILE_PATH)
        let contactList = plistRecord?.objectForKey(CONTACTLIST) as! NSMutableDictionary
        
        /**
         * Delete contact
         */
        contactList.removeObjectForKey(contact.id!)
        plistRecord?.writeToFile(PListManager.sharedInstance.PLIST_FILE_PATH, atomically: true)
    }
}
