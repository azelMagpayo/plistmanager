//
//  String+Base64.swift
//  PListManagerTestApp
//
//  Created by Glazel Magpayo on 15/07/2016.
//  Copyright © 2016 Glazel Magpayo. All rights reserved.
//

import UIKit

extension String {
    
    func base64Encoded() -> String {
        let plainData = dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String!
    }

    func  base64Decoder() -> String {
        let decodedData = NSData(base64EncodedString: self, options: NSDataBase64DecodingOptions(rawValue: 0))
        let decodedString = NSString(data: decodedData!, encoding: NSUTF8StringEncoding)
        return decodedString! as String
    }
}