//
//  ContactFormViewController.swift
//  PListManagerTestApp
//
//  Created by Glazel Magpayo on 27/06/2016.
//  Copyright © 2016 Glazel Magpayo. All rights reserved.
//

import UIKit

class ContactFormViewController: UITableViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    var contactToEdit : ContactDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkContactFromEdit()
    }
    
    func checkContactFromEdit() {
        guard let contact = contactToEdit else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: #selector(ContactFormViewController.saveContact))
            return
        }
        
        firstNameTextField.text = contact.firstName
        lastNameTextField.text  = contact.lastName
        contactTextField.text   = contact.contactNumber

        let editButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: #selector(ContactFormViewController.saveContact))
        let deleteButton = UIBarButtonItem(barButtonSystemItem: .Trash, target: self, action: #selector(ContactFormViewController.deleteContact))
        deleteButton.tintColor = UIColor.redColor()
        self.navigationItem.rightBarButtonItems = [editButton, deleteButton]
    }
    
    func saveContact() -> Void {
        if firstNameTextField.text != "" && lastNameTextField.text != "" && contactTextField.text != "" {
            guard let contact = contactToEdit else {
                /**
                 * Save NewContact
                 */
                let newContact = ContactDetails(id: nil,
                                                firstName: firstNameTextField.text!,
                                                lastName: lastNameTextField.text!,
                                                contactNumber: contactTextField.text!)
                Contact().saveNewContactDetails(newContact)
                self.navigationController?.popViewControllerAnimated(true)
                
                /**
                 * Reload contact list on home
                 */
                NSNotificationCenter.defaultCenter().postNotificationName("GETCONTACTLIST", object: nil)
                return
            }
            
            /**
             * Edit Contact
             */
            contactToEdit = ContactDetails(id: contact.id! ,
                                           firstName: firstNameTextField.text!,
                                           lastName: lastNameTextField.text!,
                                           contactNumber: contactTextField.text!)
            Contact().editContactDetails(contactToEdit!)
            self.navigationController?.popViewControllerAnimated(true)
            
            /**
             * Reload contact list on home
             */
            NSNotificationCenter.defaultCenter().postNotificationName("GETCONTACTLIST", object: nil)
        }
    }
    
    func deleteContact() {
        /**
         * Show Delete Options
         */
        let alert = UIAlertController(title: "Contacts" ,message: "Delete \(contactToEdit!.firstName) from list?", preferredStyle : .Alert)
        let deleteAction = UIAlertAction(title: "Delete", style: .Default, handler: {(alert: UIAlertAction!) in
            Contact().deleteContactDetails(self.contactToEdit!)
            self.navigationController?.popViewControllerAnimated(true)
            NSNotificationCenter.defaultCenter().postNotificationName("GETCONTACTLIST", object: nil)
            })
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {(alert: UIAlertAction!) in })
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
